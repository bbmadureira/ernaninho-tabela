var express = require('express');
var router = express.Router();

/* GET home page. */
router.get("/", function(req, res) {
  global.db.findAll((e, docs) => {
    if (e) {
      return console.log(e);
    }
    console.log(docs);
    res.render("index", { title: "Calendario", docs: docs });
  });
});


module.exports = router;
